package de.monty;


import de.monty.Methods.OnEventReceived;
import javafx.event.ActionEvent;

public class Eventhandler implements javafx.event.EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent event) {

        if (event.getSource() == Main.awoo) {
                OnEventReceived rec = new OnEventReceived();
                rec.frameUpdate();
        }
    }
}
