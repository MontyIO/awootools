package de.monty;


import de.monty.Methods.OnEventReceived;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    public static  Button awoo;
    public static ProgressBar pb;
    public static Text howl;
    public static Text state;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("AwooTool v0.1-alpha");

        awoo = new Button();
        awoo.setText("Awoo");
        awoo.setPrefSize(350, 50);
        awoo.setLayoutX(80);
        awoo.setLayoutY(50);
        awoo.setFont(Font.font("Calibri", 20));
        awoo.setDisable(false);
        awoo.setOnAction(new Eventhandler());

        pb = new ProgressBar(0.0);
        pb.setPrefSize(400, 10);
        pb.setLayoutX(50);
        pb.setLayoutY(220);

        howl = new Text();
        howl.setText("Click to start a howl");
        howl.setFont(Font.font("Calibri", 16));
        howl.setLayoutX(190);
        howl.setLayoutY(130);

        state = new Text();
        state.setText("Ready");
        state.setFont(Font.font("Calibri", 16));
        state.setLayoutX(50);
        state.setLayoutY(210);

        Pane layout = new Pane();
        layout.getChildren().add(awoo);
        layout.getChildren().add(pb);
        layout.getChildren().add(howl);
        layout.getChildren().add(state);

        Scene scene = new Scene(layout, 500, 300);
        primaryStage.setScene(scene);
        primaryStage.setAlwaysOnTop(false);
        primaryStage.setResizable(false);
        primaryStage.show();

    }
}
