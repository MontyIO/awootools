package de.monty.Methods;

import de.monty.Main;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.concurrent.TimeUnit;

public class OnEventReceived {

    public int i;

    public void frameUpdate() {
        new Thread(() -> {
        Platform.runLater(() ->Main.awoo.setDisable(true));
        Platform.runLater(() ->Main.howl.setText("Awooing..."));
        Platform.runLater(() ->Main.howl.setLayoutX(225));


                try {
                    Platform.runLater(() -> Main.pb.setProgress(0.1));
                    Platform.runLater(() -> Main.state.setText("Fetching the stick..."));
                    TimeUnit.SECONDS.sleep(1);

                    Platform.runLater(() -> Main.pb.setProgress(0.2));
                    TimeUnit.SECONDS.sleep(1);

                    Platform.runLater(() -> Main.pb.setProgress(0.3));
                    Platform.runLater(() -> Main.state.setText("updating awoo-license..."));
                    TimeUnit.SECONDS.sleep(1);

                    Platform.runLater(() -> Main.pb.setProgress(0.4));
                    Platform.runLater(() -> Main.state.setText("Initiating Howlengine..."));
                    TimeUnit.SECONDS.sleep(1);


                    Platform.runLater(() -> Main.pb.setProgress(0.5));
                    Platform.runLater(() -> Main.state.setText("Evacuating the felines..."));
                    TimeUnit.SECONDS.sleep(1);


                    Platform.runLater(() -> Main.pb.setProgress(0.6));
                    TimeUnit.SECONDS.sleep(1);


                    Platform.runLater(() -> Main.pb.setProgress(0.7));
                    TimeUnit.SECONDS.sleep(1);


                    Platform.runLater(() -> Main.pb.setProgress(0.8));
                    Platform.runLater(() -> Main.state.setText("*inhaling*..."));
                    TimeUnit.SECONDS.sleep(1);


                    Platform.runLater(() -> Main.pb.setProgress(0.9));
                    TimeUnit.SECONDS.sleep(1);


                    Platform.runLater(() -> Main.pb.setProgress(1));
                    Platform.runLater(() -> Main.state.setText("Awooo!"));
                    Platform.runLater(() -> Main.howl.setText("Done."));
                    Platform.runLater(() -> Main.howl.setLayoutX(235));

                    for (int i = 0; i<10; i++) {
                        Platform.runLater(() -> awooBox(Randomizer.randomDouble(1500, 500),
                        Randomizer.randomDouble(800, 200)));
                    }

                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
            }).start();
    }

    private void awooBox (double px, double py) {

        Stage window = new Stage();

        Button ok = new Button("Ok");
        ok.setFont(Font.font("Calibri", 10));
        ok.setLayoutX(150);
        ok.setLayoutY(80);
        ok.setMinSize(50, 20);
        ok.setMaxSize(50, 20);
        ok.setOnAction(e -> window.close());

        Label label = new Label("Awooooooooooooooooo\noooooooooooo!");
        label.setLayoutX(10);
        label.setFont(Font.font("Calibri", 16));

        Pane layout = new Pane();
        layout.getChildren().addAll(ok, label);


        window.setScene(new Scene(layout, 200, 100));
        window.setX(px);
        window.setY(py);
        window.setTitle("Awoooooooooooooooooooo!");
        window.setResizable(false);
        //window.initModality(Modality.APPLICATION_MODAL); Not a good idea
        window.setAlwaysOnTop(true);
        window.showAndWait();
    }
}
