package de.monty.Methods;

import java.util.Random;

public class Randomizer {

    public static double randomDouble (int upperRange, int lowerrange) {
        Random random = new Random();

        int i = random.nextInt((upperRange - lowerrange) + 1) + lowerrange;

        double d = Double.valueOf(i);

        return d;
    }
}
